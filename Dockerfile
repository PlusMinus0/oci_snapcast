FROM docker.io/rust:1.85.0-alpine AS builder

# renovate: datasource=github-tags depName=librespot-org/librespot
ENV LIBRESPOT_VERSION=v0.6.0
ARG REF=tags/${LIBRESPOT_VERSION}

ADD https://github.com/librespot-org/librespot/archive/refs/${REF}.tar.gz librespot.tar.gz

RUN apk add musl-dev \
	&& mkdir -p /librespot \
	&& tar -xzf librespot.tar.gz --strip-components=1 -C /librespot \
	&& cd librespot \
	&& cargo build --release --no-default-features



FROM docker.io/alpine:3.21

RUN apk add snapcast-server=0.29.0-r0 --repository http://dl-cdn.alpinelinux.org/alpine/edge/community
#    && apk add librespot --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/
COPY --from=builder /librespot/target/release/librespot /usr/local/bin/

VOLUME /etc/snapserver/

EXPOSE 1704
EXPOSE 1705 

CMD  ["snapserver", "-c", "/etc/snapserver/snapserver.conf"]

